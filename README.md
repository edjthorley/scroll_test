This repository is a programming challenge by The Secret Police. To get started please **Fork** the repository into your own BitBucket account before making any commits.

It contains a Unity project (v5.3.4) written in C#.

Open the Main Menu scene and run it in the IDE.
You should see a scroll view with 1000 items in that can be scrolled vertically.
Each item contains an image button and a TextMeshPro object.
Each image is unique on purpose.

If you do an Android build, you'll notice that the scrolling performance is really bad.

We would like you to come up with a better solution than Unity's default Scroll View.

Things to consider:
* maybe you don't need to instantiate 1000 prefabs,
* maybe you don't need all those Rect Transforms,
* minimising draw calls

We want an Android build of the Main Menu scene where it is running at 60 FPS as a minimum. We are obviously also looking at the quality of the code.

Feel free to ask us questions as you go.